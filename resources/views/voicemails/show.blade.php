@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Voicemail - {{ $voicemail->file_name }}</div>

                    <div class="card-body">
                        <table class="table table-hover">
                            <audio controls controlsList="nodownload">
                                <source src="horse.ogg" type="audio/ogg">
                                <source src="{{ $voicemail->get_file_uri() }}" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
