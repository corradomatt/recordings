@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Voicemails</div>

                    <div class="card-body">
                        <table class="table table-hover">
                            @foreach ($voicemails as $voicemail)
                                <tr>
                                    <td>{{ $voicemail->created_at }}</td>
                                    <td>
                                        <div>
                                            <audio controls controlsList="nodownload">
                                                <source src="horse.ogg" type="audio/ogg">
                                                <source src="{{ $voicemail->get_file_uri() }}" type="audio/mpeg">
                                                Your browser does not support the audio element.
                                            </audio>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $voicemail->created_at->diffForHumans() }}
                                    </td>
                                    <td>
                                        <form action="{{ action('VoicemailsController@destroy', $voicemail) }}" method="post" class="inline-form">
                                            @csrf
                                            <button type="submit" class="btn btn-link">Delete</button>
                                        </form>
                                    </td>
                                    <td>
                                        <a href="{{ $voicemail->path() }}"><i class="fas fa-link"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
