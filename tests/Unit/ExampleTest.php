<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function voicemail_has_file_name()
    {
        $voicemail = factory('App\Voicemail')->create();

        $this->assertNotEmpty($voicemail->file_name);
    }
}
