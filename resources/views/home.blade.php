@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="cards">
                {{--<div class="card-header">Dashboard</div>--}}

                <div class="cards-body text-center">
                    @foreach($voicemail as $audio)
                        <div>
                            <audio controls controlsList="nodownload">
                                {{--<source src="horse.ogg" type="audio/ogg">--}}
                                <source src="{{ $audio->get_file_uri() }}" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
