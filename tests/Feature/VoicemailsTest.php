<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VoicemailsTest extends TestCase
{
    use DatabaseMigrations;

    protected $voicemail;

    public function setUp()
    {
        parent::setUp();
        $this->voicemail = factory('App\Voicemail')->create();
    }

    /**
     * @test
     * @return void
     */
    public function a_user_can_browse_voicemails()
    {
        $response = $this->get('/voicemails');
        $response->assertStatus(200);
        $response->assertSee($this->voicemail->file_name);
    }

    /**
     * @test
     * @return void
     */
    public function a_user_can_see_a_single_voicemail()
    {
        $response = $this->get('/voicemail/' . $this->voicemail->id);
        $response->assertStatus(200);
        $response->assertSee($this->voicemail->file_name);
    }


    /**
     * @test
     * @return void
     */
    public function a_registered_user_can_add_a_voicemail()
    {
        $this->json('POST', '/voicemail/')
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function a_valid_voicemail_must_be_provided()
    {
        $this->be(new User()); // @TODO: fix this authentication stub

        $this->json('POST', '/voicemail/', [
            'file' => 'not-a-voicemail',
        ])->assertStatus(422);
    }

    /**
     * @test
     */
    public function a_voicemail_may_be_uploaded()
    {
        $this->be(new User()); // @TODO: fix this authentication stub

        Storage::fake('public');

        // @TODO: retrieve this response with data to check to make sure the image is associated correctly
        // @link https://laracasts.com/series/lets-build-a-forum-with-laravel/episodes/63
        $this->json('POST', '/voicemail/', [
            'voicemail' => $file = UploadedFile::fake()->create('faker-test.mp3'),
        ]);

        Storage::disk('public')->assertExists("voicemails/{$file->hashName()}");
    }
}
