<?php

namespace App\Http\Controllers;

use App\Voicemail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VoicemailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voicemails = Voicemail::latest()->get();

        return view('voicemails.index', compact('voicemails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'voicemail' => ['required', 'file']
        ]);

        $uploaded_voicemail = $request->file('voicemail');

        $voicemail = Voicemail::create([
            'user_id' => auth()->id(),
            'file_name' => $uploaded_voicemail->hashName(),
            'file_ext' => $uploaded_voicemail->extension(),
        ]);

        $uploaded_voicemail->store('voicemails', 'public');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voicemail  $voicemail
     * @return \Illuminate\Http\Response
     */
    public function show(Voicemail $voicemail)
    {
        return view('voicemails.show', compact('voicemail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voicemail  $voicemail
     * @return \Illuminate\Http\Response
     */
    public function edit(Voicemail $voicemail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voicemail  $voicemail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voicemail $voicemail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voicemail $voicemail
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voicemail $voicemail)
    {
        Storage::disk('public')->delete('voicemails/' . $voicemail->file_name);
        $voicemail->delete();

        return back();
    }
}
