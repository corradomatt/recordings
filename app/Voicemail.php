<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Voicemail extends Model
{
    protected $fillable = ['file_path', 'file_name', 'mime_type'];
    private $storage_path = 'public/voicemails/';


    public function path()
    {
        return '/voicemail/' . $this->id;
    }


    public function get_file_path()
    {
        return storage_path($this->storage_path . $this->file_name);
    }


    public function get_file_uri()
    {
        return Storage::url($this->storage_path . $this->file_name);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
