<?php

use Illuminate\Database\Seeder;

class VoicemailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('voicemails')->insert([
            'id' => 0,
            'file_name' => 'test',
            'file_ext'  => '',
            'mime_type' => 'audio/mpeg',
            'user_id'   => null,
        ]);
    }
}
