@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload a Voicemail</div>

                <div class="card-body">
                    <form action="{{ action('VoicemailsController@store') }}" method="post" class="d-flex flex-row justify-content-between align-items-center" enctype="multipart/form-data">
                        @method('post')
                        @csrf

                        <input type="file" class="1custom-file-input" id="voicemail" name="voicemail" accept="audio/mp3, audio/ogg, audio/wav" aria-describedby="fileHelp">

                        <button type="submit" class="btn btn-primary">Upload Voicemail Message</button>
                    </form>
                    <p class="mt-4 mb-0"><i>*Supported files types are <em>MP3</em>, <em>OGG</em> and (shudder) <em>WAV</em> files.</i></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
