<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = [
        'voicemail' => \App\Voicemail::inRandomOrder()->limit(1)->get(),
    ];

    return view('home', $data);
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/upload', function() {
    return view('upload');
});

Route::get('/voicemails', 'VoicemailsController@index');
Route::get('/voicemail/{voicemail}', 'VoicemailsController@show');
Route::post('/upload', 'VoicemailsController@store')->middleware('auth');
Route::post('/voicemail/delete/{voicemail}', 'VoicemailsController@destroy')->middleware('auth');
